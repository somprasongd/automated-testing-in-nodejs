const db = require('./db');

// Testing Number
module.exports.absolute = number => {
  if (number > 0) return number;
  if (number < 0) return -number;
  return 0;
}

// Testing String
module.exports.greet = name => {
  return `Hello ${name}!`;
}

// Testing Array
module.exports.getGenders = () => {
  return ['Male', 'Female', 'N/A'];
}

// Testing Object
module.exports.getPet = (petId) => {
  return {
    id: petId,
    name: 'Stamp',
    age: 4
  };
}

// Testing Exceptions
module.exports.registerUser = (username) => {
  if (!username) throw new Error('Username is required.');

  return {id: new Date().getTime(), username: username};
}

// Testing Mock Functions
module.exports.applyDiscount = async(order) => {
  const customer = await db.getCustomer(order.customerId);

  if(customer.points > 10) {
    // discount 10%
    order.totalPrice *= 0.9;
  }
}