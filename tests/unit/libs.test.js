const libs = require('../../src/libs');
const db = require('../../src/db');

describe('absolute', () => {
  it('should return a positive number if input is positive', () => {
    // Arrange
    const input = 1;
    // Action
    const result = libs.absolute(input);
    // Assert
    expect(result).toBe(input);
  });

  it('should return a positive number if input is negative', () => {
    const input = -1;
    const result = libs.absolute(input);
    expect(result).toBe(1);
  });

  it('should return 0 if input is 0', () => {
    const input = 0;
    const result = libs.absolute(input);
    expect(result).toBe(input);
  });
});

describe('greet', () => {
  it('should return the greeting message', () => {
    const name = 'Ball';
    const result = libs.greet(name);
    expect(result).toMatch(/Ball/);
    expect(result).toContain('Ball');
  })
});

describe('getGenders', () => {
  it('should return all genders', () => {
    const result = libs.getGenders();
    expect(result).toBeDefined();
    expect(result).not.toBeNull();
    expect(result).toContain('Male');
    expect(result).toEqual(expect.arrayContaining(['Female', 'Male', 'N/A']));
  })
});

describe('getPet', () => {
  it('should return the pet with the given id', () => {
    const petId = 1;
    const result = libs.getPet(petId);
    expect(result).toMatchObject({id: petId, name: 'Stamp'});
    expect(result).toHaveProperty('id');
  })
});

describe('registerUser', () => {
  it('should throw if username is falsy', () => {
    // falsy in javascript
    const args = [null, undefined, NaN, '', 0, false];
    args.forEach(a => {
      expect(() => { libs.registerUser(a) }).toThrow();
    });
  });

  it('should return a user object if valid username is passed', () => {
    const username = 'Ball';
    const result = libs.registerUser(username);
    expect(result).toMatchObject({ username: username});
    expect(result.id).toBeGreaterThan(0);
  });
});

describe('applyDiscount', () => {
  it('should apply 10% discount if customer has more than 10 points', async () => {
    // mock functions
    db.getCustomer = jest.fn();
    db.getCustomer.mockResolvedValue({ 
      id: 1, 
      points: 20});

    const order = { 
      customerId: 1,
      totalPrice: 10
    }
    await libs.applyDiscount(order);
    expect(order.totalPrice).toBe(9);
  });
});